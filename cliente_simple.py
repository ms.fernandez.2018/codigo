import socket


s=socket.socket(socket.AF_INET, socket.SOCK_STREAM) #método de socket que recibe params que definen el tipo de socket
# AF_INET => internet, pero tenemos muchas opciones de hacer sockets para conectarse a muchos servidores
# SOCK_STREAM es el tipo de flujo que van a tener los datos, en este caso stream es un flujo constnate de comunicación
print()
print("Socket creado")
print(s)
#al impirmir nos sale laddr = (...,...) que son dirección y el puerto

s.connect(("www.urjc.es",80))
print()
print(s)
#ahora a parte de darnos en laddr nuestra dirección ip desde la que nos conectamos, con el puerto de nuestro equipo que estamos utilizando
#nos devuelve raddr que es la dirección y puerto al que nos conectamos
