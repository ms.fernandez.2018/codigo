import socket

PORT = 8080 #los demás son puertos reservados
IP = "127.0.0.1" #dirección ip de tu ordenador
MAX_OPEN_REQUESTS = 2

# Numero de conexiones establecidas, para llevar la cuenta
# en la pantalla
n = 0

# Crear un socket para recibir peticiones
serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

#el servidor es al que nos conectamos, este no se conecta a nada es pasivo => bind y listen

# Obtener el nombre del servidor, del sistema
# hostname = socket.gethostname()
try:
    # Asociar el socket a este servidor y el puerto elegido
    serversocket.bind((IP, PORT)) #asociamos el socket al ip y puerto que queramos

    # Configurar el socket: modo servidor
    serversocket.listen(MAX_OPEN_REQUESTS) #le decimos al objeto que espere conexiones, un máximo para que no se sature
    #si es del mismo cliente (mismo IP) se puede conectar las veces que quiera
    while True:  #lo mantiene funcionando hasta que se termine el programa externmanete

        # Esperar conexiones
        print()
        print("Esperando Conexiones en {} {}".format(IP, PORT))
        (clientsocket, address) = serversocket.accept() #le decimos al objeto servercket que hemos creado que permita la conexión, nos devuelve el cliente que se ha conectado
        #se queda bloqueado en esta línea hasta que se haya conectado un cliente
        #clientesocket al igual que el servecocket nos da la comunicación con el cliente

        # Se ha recibido una conexión de un cliente
        # Incrementar el numero de conexiones y mostrar informacion
        # sobre la conexion recibida
        n += 1
        print("Conexion {}".format(n))
        print(clientsocket)
        print("Conexion desde: {}".format(address))

except socket.error as ex: #socket es la biblioteca el .() es la clase
    print("Problemas using port %i. Do you have permission?" % PORT)
    print()
    print(ex)