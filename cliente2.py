import socket

PORT = 8080 #los demás son puertos reservados
IP = "127.0.0.1" #dirección ip de tu ordenador

s=socket.socket(socket.AF_INET, socket.SOCK_STREAM)

print()
print("Socket creado")
print(s)


try:
    s.connect((IP,PORT)) #recibe host y puerto, tenemos que pasarlo en forma de tupla
except OSError:
    print("Socket no está disponible")
    #Así que nos desconectamos para poder probar otra cosa
    s.close()
    #Y volvemos a definir el socket
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((IP, PORT))

#Si ha sido exitosa
print("Read from the server", s.recv(2048).decode("utf-8"))


