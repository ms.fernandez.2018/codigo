#model matter

#1 atoms => electrones, protones, neutrones...=> nuestro primer concepto (contienen las caracterísitcas comunes a todos los objetos de esa calse) del que se crean objetos

class Atom():
    #también podríamos dar valores globales sin el init
    #electrons=...
    #protons=...
    #hay veces que no quieres tener atributos específicos de objeto sino que sean comunes a todas las instancias => comportamientos de clase = classmethod
    #electrons=None => NO SE DECLARA CON SELF PORQUE NO SE CONSTRUYE COMO OBJETO, ES PARTE DE LA CLASE (CLS EN VEZ DE SELF)
    #protons=None
    # def __init__(self, neutrons):
    #     self.neutrons = neutrons
    #
    # @classmethod
    # def get_electrons(cls): variables compartidas a nivel de clase => cls, le pasamos la clase, no el objeto
    #     print(cls)
    #     return cls.electrons
    #
    # @classmethod
    # def get_protons(cls):
    #     return cls.protons
    #
    # def get_neutrons(self):
    #     return self.neutrons
    # métodos privados (uno de los principios SOLID), no parte de la api (parte pública a la que se puede accder)
    # def __init__(self, neutrons=None):
    #     self.neutrons = neutrons
    #     self.__atom_private()
    #
    # def __atom_private(self):
    #     print("Atom private method")
    #
    # class Carbon(Atom):
    #     electrons = 6
    #     protons = 6
    #
    #     def __init__(self, neutrons=6):
    #         self.neutrons = neutrons
    #         self.__atom_private()

    def __init__(self,electrons,protons): #constructor de objetos por defecto, self es una variable que se genera automáticamente al crear objeto (se refiere así mismo)
        self.electrons=electrons #al ser el constructor las características comunes a todos los objetos que construya las definimos aquí
        self.protons=protons

    def get_electrons(self): #las cosas que van a hacer los objetos
        return self.electrons #tiene esta forma con el self porque se refieren al atributo de sí mismo

    def get_protons(self):
        return self.protons

atom= Atom(electrons=6,protons=6) #instancia de átomo (es el objeto concreto)

print(atom.get_electrons())
print(atom.get_protons())

#podemos crear una clase a partir de otras clases (especializar las clases = herencia)

class Carbon(Atom): #carbon hereda los atributos y lo que hacen los objetos atom=> podemos especificar los atributos y ya tenemos un "tipo de atom"
    def __init__(self):
        self.electrons=6
        self.protons=6

carbon=Carbon()

print(carbon.get_electrons(),carbon.get_protons())